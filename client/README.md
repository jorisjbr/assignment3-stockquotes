Document all concepts and your implementation decisions.


#Flow of the program
TODO: Improve this flow
1. init
1. retrieve data
1. draw data
1. go to step 2


#Concepts
For every concept the following items:
- short description
- code example
- reference to mandatory documentation
- reference to alternative documentation (authoritive and authentic)

###Objects, including object creation and inheritance
TODO:

###websockets
TODO:

###XMLHttpRequest
TODO:

###AJAX
TODO:

###Callbacks
TODO:

###How to write testable code for unit-tests
TODO:


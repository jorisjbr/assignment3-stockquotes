/*jslint browser: true, plusplus:true*/

describe("scenario", function () {

    var properties, methods;

    beforeEach(function () {
        var propertyName;

        app.init();

        properties = [];
        methods = [];
        for (propertyName in app) {
            if (app.hasOwnProperty(propertyName)) {
                if (app[propertyName].constructor === Function) {
                    methods.push(app[propertyName].prototype);
                } else {
                    properties.push(propertyName);
                }
            }
        }
    });
    afterEach(function () {
        document.body.removeChild(document.querySelector("#container"));
    });

    it("Should verify that the app has properties and methods.", function () {
        expect(properties.length).not.toBe(0);
        expect(methods.length).not.toBe(0);
    });
    //tests addToSeries
    it("Should verify that app.series is not empty", function () {
        var expectedvalue, actualvalue;
        expectedvalue = 0;
        actualvalue = app.series.length;
        expect(actualvalue).not.toBe(expectedvalue);
    });
    //tests initHTML
    it("Should verify that the header contains the correct text", function () {
        var expectedvalue, actualvalue;
        expectedvalue = "Real time stockquote app";
        actualvalue = app.initHTML().querySelector("h1").innerText;
        expect(actualvalue).toBe(expectedvalue);
    });
    //tests ShowData
    it("Should verify that the table is 25 rows", function(){
        var expectedvalue, actualvalue;
        expectedvalue = 25;
        actualvalue = app.showData().querySelectorAll("tr").length;
        expect(actualvalue).toBe(expectedvalue);
    });
    it("Should verify retrieving realtime data cannot be tested", function () {
        var actualValue = app.getRealtimeData();
        var expectedValue = undefined;
        expect(actualValue).toBe(expectedValue);
    });
    it("Should verify that ranges including min and max values are returned", function () {
        var input = 100, range = 5, hitMin = 0, hitMax = 0, i, r;

        for (i = 0; i < 1000; i++) {
            r = app.rnd(input, range);
            if (r === input - range) {
                hitMin++;
            } else if (r === input + range) {
                hitMax++;
            }
        }

        console.log('hitMin', hitMin);
        console.log('hitMax', hitMax);

        expect(hitMin).toBeGreaterThan(0);
        expect(hitMax).toBeGreaterThan(0);
    });
});
(function () {
    "use strict";
    window.app = {

        series: {},

        socket: io("http://server7.tezzt.nl:1333"),

        settings: {
            refresh: 1000,
            ajaxUrl: "http://server7.tezzt.nl/~theotheu/stockquotes/index.php",
            datapoints: 100
        },

        rnd: function (min, max) {
            return (Math.random() * (max - min)) + min;
        },

        getFormattedDate: function (d) {
            var res = "";
            res += d.getDate();
            res += "/";
            res += d.getMonth();
            res += "/";
            res += d.getFullYear();
            return res;
        },

        getFormattedTime: function (d) {
            var hour, minute, suffix;

            hour = d.getHours();
            minute = d.getMinutes();
            suffix = "AM";
            if (hour > 12) {
                hour -= 12;
                suffix = "PM";
            }
            return hour + ":" + minute + suffix;
        },

        generateTestData: function () {
            var company, quote, newQuote;

            for (company in window.app.series) {
                if (window.app.series.hasOwnProperty(company)) {
                    quote = window.app.series[company][0];
                    newQuote = Object.create(quote);
                    newQuote.col1 = window.app.rnd(10, 20);
                    newQuote.col2 = window.app.getFormattedDate(new Date());
                    newQuote.col3 = window.app.getFormattedTime(new Date());
                    newQuote.col4 = -1 + Math.floor(Math.random() * 3); // difference of price value between this quote and the previous quote

                    window.app.series[company].unshift(newQuote);
                }
            }
        },

        parseData: function (rows) {
            var i, company;

            // Iterate over the rows and add to series
            for (i = 0; i < rows.length; i++) {
                company = rows[i].col0;

                // Check if array for company exist in series
                if (window.app.series[company] !== undefined) {
                    window.app.series[company].unshift(rows[i]);
                } else {
                    // company does not yet exist
                    window.app.series[company] = [rows[i]];
                }
            }
        },

        retrieveData: function () {
            var xhr = new XMLHttpRequest();
            xhr.open("GET", "data.js", false);
            xhr.send();
            window.app.retrieveRows(xhr);
        },
        retrieveRows: function (e) {
            var str, obj, rows;
            str = e.target.responseText;
            obj = JSON.parse(str);
            rows = obj.request.result.row;
            window.app.addToSeries(rows);
        },
        addToSeries: function (q) {
            window.app.series[q].unshift(q);
            if (window.app.series[q].length > window.app.settings.datapoints) {
                window.app.series.shift();
            }
        },
        createValidCSSNameFromCompany: function (str) {
            // regular expression to remove everything
            // that is not out of A-z0-9
            return str.replace(/\W/, "");
        },

        showData: function () {
            // return value is a dom
            var Tbody, icon, company, row, quote, cell, propertyName, propertyValue;

            Tbody = document.querySelector("#myTbody");

            // remove old data
            if (Tbody.children !== null) {
                Tbody.innerHTML = "";
            }

            // Create rows
            for (company in window.app.series) {
                if (window.app.series.hasOwnProperty(company)) {
                    quote = window.app.series[company][0];
                    row = document.createElement("tr");
                    row.id = window.app.createValidCSSNameFromCompany(company);

                    icon = document.createElement("span");

                    if (quote.col4 < 0) {
                        row.className = "alert alert-danger";
                        icon.className = "glyphicon glyphicon-arrow-down";
                    } else if (quote.col4 > 0) {
                        row.className = "alert alert-success";
                        icon.className = "glyphicon glyphicon-arrow-up";
                    } else {
                        row.className = "default";
                        icon.className = "glyphicon glyphicon-minus";
                    }
                    // Create cells
                    Tbody.appendChild(row);

                    // Iterate over quote to create cells
                    for (propertyName in quote) {
                        if (quote.hasOwnProperty(propertyName)) {
                            propertyValue = quote[propertyName];
                            cell = document.createElement("td");
                            cell.innerText = propertyValue;
                            row.appendChild(cell);
                        }
                    }
                    row.appendChild(icon);
                }
            }
        },

        getRealtimeData: function () {
            window.app.socket.on('stockquotes', function (data) {
                window.app.parseData(data.query.results.row);
            });
        },

        loop: function () {
            //window.app.generateTestData();

            // show or update the data in the table
            window.app.showData();

            setTimeout(window.app.loop, window.app.settings.refresh);
        },

        initHTML: function () {
            var container, h1Node, headers, th, i;

            // Create container
            container = document.querySelector(".panel");

            window.app.container = container;

            // Create title of application
            h1Node = document.createElement("h1");
            h1Node.innerText = "Real Time Stockquote App";
            //h1Node.className = "panel-heading";
            document.querySelector(".panel-heading").appendChild(h1Node);

            // Create table header
            headers = ["Company", "Last trade", "Date of last trade", "Time of last trade", "Change", "Open price", "Day's high", "Day's low", "Volume", "Icon"];
            for (i = 0; i < headers.length; i++) {
                th = document.createElement("th");
                th.innerText = headers[i];
                document.querySelector("#myThead").appendChild(th);
            }
        },

        init: function () {
            window.app.initHTML();
            window.app.getRealtimeData();
            window.app.loop();
        }
    };
}());